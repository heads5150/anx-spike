﻿using BootstrapTest.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BootstrapTest.Filters
{
    public class ActionLevelFilterAttribute : ActionFilterAttribute
    {
        public ICoreService CoreService { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            filterContext.Controller.TempData["HelloWorld"] = CoreService.GetHelloWorld();
        }
    }

    public class GlobalActionLevelFilterAttribute : ActionFilterAttribute
    {
        public ICoreService CoreService { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            filterContext.Controller.TempData["GlobalHelloWorld"] = CoreService.GetHelloWorld() + "Global";
        }
    }
}