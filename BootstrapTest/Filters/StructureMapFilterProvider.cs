﻿using BootstrapTest.App_Start;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BootstrapTest.Filters
{
    public class StructureMapFilterProvider :  FilterAttributeFilterProvider
    {
        private IContainer _container;

        public StructureMapFilterProvider(IContainer container)
        {
            _container = container;
        }

        public override IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            var filters = base.GetFilters(controllerContext, actionDescriptor);
            foreach (var filter in filters)
            {
                _container.BuildUp(filter.Instance);
            }
            return filters;
        }
        //public IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        //{
        //    var container = StructuremapMvc.StructureMapDependencyScope.CurrentNestedContainer;
        //    var filters = base.GetFilters(controllerContext, actionDescriptor);

        //    foreach (var filter in filters)
        //    {
        //        container.BuildUp(filter);
        //    }

        //    return filters;
        //}
    }


    public class GlobalFilterRegistration
    {
        public Type Type { get; set; }
        public int? Order { get; set; }
    }

    public class GlobalFilterRegistrationList : List<GlobalFilterRegistration>
    {
    }
    public class StructureMapGlobalFilterProvider : IFilterProvider
    {
        public StructureMapGlobalFilterProvider(IContainer container, GlobalFilterRegistrationList filterList)
        {
            this.container = container;
            this.filterList = filterList;
        }
        private readonly IContainer container;
        private readonly GlobalFilterRegistrationList filterList;

        public IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor
    actionDescriptor)
        {
            var filters = new List<Filter>();
            if (filterList == null || filterList.Count == 0)
                return filters;
            filters.AddRange(from registration in filterList let actionFilter = container.GetInstance(registration.Type) select new Filter(actionFilter, FilterScope.Global, registration.Order));

            return filters;
        }
    }
}