﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using BootstrapTest.Filters;

namespace BootstrapTest
{
    public class FilterConfig
    {
        public static ActionFilterAttribute[] GlobalFilterRegistrationList
        {
            get { return new ActionFilterAttribute[] { new GlobalActionLevelFilterAttribute() }; }
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            foreach (ActionFilterAttribute filter in GlobalFilterRegistrationList)
            {
                filters.Add(filter);
            }
        }

    }
}
