// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Runtime.InteropServices.ComTypes;

namespace BootstrapTest.DependencyResolution {
    using BootstrapTest.App_Start;
    using BootstrapTest.Core;
    using BootstrapTest.Filters;
    using StructureMap;
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using System.Web.Mvc;
	
    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.AssemblyContainingType<CoreRegistry>();
                    scan.LookForRegistries();
                    scan.WithDefaultConventions();
					scan.With(new ControllerConvention());
                });
            //For<IExample>().Use<Example>();
            
            For<IFilterProvider>().Use<StructureMapFilterProvider>();

            var list = new GlobalFilterRegistrationList();
            var i = 1;
            foreach (ActionFilterAttribute filter in FilterConfig.GlobalFilterRegistrationList)
            {

                    list.Add(new GlobalFilterRegistration
                    {
                        Order=i,Type=filter.GetType()
                    });
                
            i++;
            }

            For<GlobalFilterRegistrationList>().Use(list);
            For<IFilterProvider>().Use<StructureMapGlobalFilterProvider>();

            //set up setter policies for filters
            Policies.SetAllProperties(p => p.OfType<ICoreService>());
           
        }

        #endregion
    }
}