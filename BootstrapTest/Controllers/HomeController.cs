﻿using BootstrapTest.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BootstrapTest.Controllers
{
    [GlobalActionLevelFilter]
    public class HomeController : Controller
    {
        [ActionLevelFilter]
        public ActionResult Index()
        {
            string s = TempData["HelloWorld"].ToString();
            string t = TempData["GlobalHelloWorld"].ToString();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}