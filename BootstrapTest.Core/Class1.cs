﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootstrapTest.Core
{
    public interface ICoreService
    {
        string GetHelloWorld();
    }

    public class CoreService : ICoreService
    {

        public string GetHelloWorld()
        {
            return "Hello World";
        }
    }
}
